﻿namespace StickyNotes
{
    public class GlobalSettings
    {
        public int positionX;
        public int positionY;
        public int scaleX;
        public int scaleY;
        public int fontSize;
        public string colorNote;
        public string colorFont;
        public bool isBold;
        public bool isItalic;
    }
}
