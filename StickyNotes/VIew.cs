﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StickyNotes
{
    public class View
    {
        public bool isApplicationVisible;
        public Dictionary<string, bool> elementsVisibility = new Dictionary<string, bool>();

        public void MinimizeApplication()
        {

        }

        public void MaximizeApplication()
        {

        }

        public void SetVisibility(string controlId, bool isVisible)
        {

        }

        public void RefreshView()
        {

        }

    }
}
