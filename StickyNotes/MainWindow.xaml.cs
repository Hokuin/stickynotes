﻿using System;
using System.Windows;

namespace StickyNotes
{

    //Taskbar
    public partial class MainWindow : Window
    {
        ApplicationManager appManager;

        public MainWindow()
        {
            appManager = new ApplicationManager();
            appManager.Initialize();

            InitializeComponent();
            Top = SystemParameters.PrimaryScreenHeight / 3 * 2;
            Left = (SystemParameters.PrimaryScreenWidth - Width) / 2;

            this.StateChanged += new EventHandler(OnWindowStateMinimized);
            this.StateChanged += new EventHandler(OnWindowStateNormal);
        }

        private void CreateNewNote(object sender, RoutedEventArgs e)
        {
            appManager.AddNote();
        }

        private void OpenSettings(object sender, RoutedEventArgs e)
        {
            appManager.OpenSettings();
        }

        private void HideApplication(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void CloseApplication(object sender, RoutedEventArgs e)
        {
            appManager.CloseApplication();
            this.Close();
        }


        #region on main window state change custom events
        public void OnWindowStateMinimized(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Minimized)
                appManager.HideApplicationWindows();
        }

        public void OnWindowStateNormal(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
                appManager.ShowApplicationWindows();
        }
        #endregion
    }
}
